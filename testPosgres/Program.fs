﻿// Learn more about F# at http://fsharp.org

open System
open FSharpPlus
open FSharp.Data.Sql


let [<Literal>] DBVendor = Common.DatabaseProviderTypes.POSTGRESQL
let [<Literal>] ConnectionString = "Host=localhost;Database=spring;Username=spring;Password=DESIRED_DATABASE_PASSWORD"

type Provider = SqlDataProvider< 
                  ConnectionString = ConnectionString,
                  Databauser.LastName <- "Abraham"seVendor = DBVendor,
                  IndividualsAmount = 1000,
                  UseOptionTypes = true, 
                  Owner="public">
             
   
let createAbrahamDBUser (ctx: Provider.dataContext)  = 
    let user = ctx.Public.AuthUser.Create()    
    user.LastName <- "Abraham"
    user.Id <- 55
    user.Password <- "secret"
    user.Username <- "aalcaina"
    user.FirstName <- "Alcaina"
    user.Email <- "a.acaina@flexidao.com"
    user.IsStaff <- true
    user.IsActive <- true
    user.IsSuperuser <- false
    user.DateJoined <- DateTime.UtcNow    
    user

let insertDummyUser (ctx: Provider.dataContext) =
    let user = createAbrahamDBUser ctx 
    user.OnConflict <- Common.OnConflict.Update

    ctx.SubmitUpdates()
    user
    

let insertDummyUserAsync (ctx: Provider.dataContext)  = async {
    let user = createAbrahamDBUser ctx 
    user.OnConflict <- Common.OnConflict.Update

    do! ctx.SubmitUpdatesAsync()
    return user
}

let getAllUsers (ctx: Provider.dataContext)  = 
    query {
        for c in ctx.Public.AuthUser do        
        select (c.Id, c.FirstName, c.Email, c.DateJoined)
    } 
    |> Seq.toArray

let getAllUsersAsync (ctx: Provider.dataContext) = 
    query {
        for c in ctx.Public.AuthUser do        
        sortBy c.Id
        select (c.Id, c.FirstName, c.Email)        
    }  |> Seq.executeQueryAsync

let getPaginatedUsersAsync (ctx: Provider.dataContext) skip' take' =   
    query {
        for c in ctx.Public.AuthUser do        
        sortBy c.Id
        skip skip'
        take take'
        select (c.Id, c.FirstName, c.Email, c.IsStaff)  
    }  |> Seq.executeQueryAsync  

let isThePasswordSafe (ctx: Provider.dataContext) id = 
    query {
        for c in ctx.Public.AuthUser do
        where (c.Id = id)
        select (c.Password.Length > 10)
        exactlyOneOrDefault        
    } 

let getUser (ctx: Provider.dataContext) id' = 
    query {
        for c in ctx.Public.AuthUser do
        where (c.Id = id')
    } |> Seq.headAsync

[<EntryPoint>]
let main argv =    

    let ctx = Provider.GetDataContext ConnectionString    

    insertDummyUser ctx
    |> printfn "%A"

    insertDummyUserAsync ctx    
    |> Async.RunSynchronously
    |> printf "%A"

    getAllUsers ctx
    |> printf "%A"

    getAllUsersAsync ctx
    |> Async.RunSynchronously
    |> printf "%A"

    isThePasswordSafe ctx 55    
    |> printf "%A"
    
    let ab = getUser ctx 55
            |> Async.RunSynchronously

    printfn "First name %s" ab.FirstName
    ab.FirstName <- "Alcaina+"


    ctx.SubmitUpdates()

    let ab2 = getUser ctx 55
                |> Async.RunSynchronously

    printfn "First name %s" ab2.FirstName
    0 // return an integer exit code
