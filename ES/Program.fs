﻿// Learn more about F# at http://fsharp.org

open System
open CosmoStore


module CmdArgs =    
    type AddTask = {        
         Id : int        
         Name : string        
         DueDate : DateTime option    
    }     
    type RemoveTask = {        
         Id : int
    }     
    type CompleteTask = {        
         Id : int    
    }     
    type ChangeTaskDueDate = {        
         Id : int        
         DueDate : DateTime option    
    }

type Command =     
    | AddTask of CmdArgs.AddTask    
    | RemoveTask of CmdArgs.RemoveTask    
    | ClearAllTasks    
    | CompleteTask of CmdArgs.CompleteTask    
    | ChangeTaskDueDate of CmdArgs.ChangeTaskDueDate


type Event =    
    | TaskAdded of CmdArgs.AddTask    
    | TaskRemoved of CmdArgs.RemoveTask    
    | AllTasksCleared    
    | TaskCompleted of CmdArgs.CompleteTask    
    | TaskDueDateChanged of CmdArgs.ChangeTaskDueDate

type Task = {    
    Id : int    
    Name : string    
    DueDate : DateTime option    
    IsComplete : bool
} 
type State = {    
    Tasks : Task list
} 
with static member Init = { Tasks = [] }    

let onlyIfTaskDoesNotAlreadyExist (state:State) i =    
    match state.Tasks |> List.tryFind (fun x -> x.Id = i) with    
    | Some _ -> failwith "Task already exists"    
    | None -> state 
let onlyIfTaskExists (state:State) i =    
    match state.Tasks |> List.tryFind (fun x -> x.Id = i) with
    | Some task -> task    
    | None -> failwith "Task does not exist" 
let onlyIfNotAlreadyFinished (task:Task) =    
    match task.IsComplete with    
    | true -> failwith "Task already finished"    
    | false -> task

let execute state command =     
    let event = 
        match command with    
        | AddTask args -> 
            args.Id 
            |> onlyIfTaskDoesNotAlreadyExist state 
            |> (fun _ -> TaskAdded args)    
        | RemoveTask args -> 
            args.Id 
            |> onlyIfTaskExists state 
            |> (fun _ -> TaskRemoved args)    
        | ClearAllTasks -> AllTasksCleared    
        | CompleteTask args -> 
            args.Id 
            |> onlyIfTaskExists state 
            |> (fun _ -> TaskCompleted args)    
        | ChangeTaskDueDate args -> 
            args.Id 
            |> (onlyIfTaskExists state >> onlyIfNotAlreadyFinished)
            |> (fun _ -> TaskDueDateChanged args)
    event |> List.singleton //we must return list of events

let apply state event =     
    match event with    
    | TaskAdded args ->         
        let newTask = { 
            Id = args.Id 
            Name = args.Name 
            DueDate = args.DueDate 
            IsComplete = false
        }        
        { state with Tasks = newTask :: state.Tasks}    
    | TaskRemoved args -> 
        { state with Tasks = state.Tasks |> List.filter (fun x -> x.Id <> args.Id) }    
    | AllTasksCleared -> { state with Tasks = [] }
    | TaskCompleted args ->         
        let task = 
            state.Tasks 
            |> List.find (fun x -> x.Id = args.Id) 
            |> (fun t -> { t with IsComplete = true })        
        let otherTasks = state.Tasks |> List.filter (fun x -> x.Id <> args.Id)        
        { state with Tasks = task :: otherTasks }    
    | TaskDueDateChanged args ->         
        let task = 
            state.Tasks 
            |> List.find (fun x -> x.Id = args.Id) 
            |> (fun t -> { t with DueDate = args.DueDate })
        let otherTasks = 
            state.Tasks 
            |> List.filter (fun x -> x.Id <> args.Id)        
        { state with Tasks = task :: otherTasks } 

type Aggregate<'state, 'command, 'event> = {    
    Init : 'state    
    Apply: 'state -> 'event -> 'state    
    Execute: 'state -> 'command -> 'event list
}

let tasksAggregate = {
    Init = State.Init
    Execute = execute
    Apply = apply
}


open Newtonsoft.Json
open Newtonsoft.Json.Converters

module Mapping = 
    let toStoredEvent a = 
        let d = JsonConvert.SerializeObject a 
        ("event", d)

    
////////////
let myConfig: Marten.Configuration =  {
    Database = "spring"
    Host = "127.0.0.1"
    Password = "c7fda242b6ce32d8d700c905c44276a068c268e2"
    Username = "spring"
}

let store: EventStore<string, int64> = Marten.EventStore.getEventStore myConfig

let getCurrentState () =        
    store.GetEvents "Tasks" EventsReadRange.AllEvents        
    |> Async.AwaitTask        
    |> Async.RunSynchronously        
    |> printfn "%A"


let append events =        
    events         
    |> List.map Mapping.toStoredEvent        
    |> List.map (fun (name,data) -> { 
            Id = Guid.NewGuid()
            CorrelationId = Guid.NewGuid() |> Some
            CausationId = None
            Name = name
            Data = data
            Metadata = None

        })        
    |> store.AppendEvents "Tasks" CosmoStore.ExpectedVersion.Any        
    |> Async.AwaitTask        
    |> Async.RunSynchronously        
    |> ignore

[<EntryPoint>]
let main argv =
    let e' = TaskAdded  {        
        Id = 1     
        Name = "Un event"
        DueDate  = None
    }     
    append [e']
    getCurrentState()
    printfn "Hello World from F#!"    

    0 // return an integer exit code    